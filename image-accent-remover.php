<?php header('Content-Type:text/html; charset=UTF-8'); ?>
<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>The image accents remover</title>
</head>

<body>



<?php /**
 * Replace language-specific characters by ASCII-equivalents.
 * @param string $s
 * @return string
 */
function normalizeChars($s) {
    $replace = array(
        '–'=>'-',
        '.'=>'-',
        'ъ'=>'-', 'Ь'=>'-', 'Ъ'=>'-', 'ь'=>'-',
        'Ă'=>'A', 'Ą'=>'A', 'À'=>'A', 'Ã'=>'A', 'Á'=>'A', 'Æ'=>'A', 'Â'=>'A', 'Å'=>'A', 'Ä'=>'Ae',
        'Þ'=>'B',
        'Ć'=>'C', 'ץ'=>'C', 'Ç'=>'C',
        'È'=>'E', 'Ę'=>'E', 'É'=>'E', 'Ë'=>'E', 'Ê'=>'E',
        'Ğ'=>'G',
        'İ'=>'I', 'Ï'=>'I', 'Î'=>'I', 'Í'=>'I', 'Ì'=>'I',
        'Ł'=>'L',
        'Ñ'=>'N', 'Ń'=>'N',
        'Ø'=>'O', 'Ó'=>'O', 'Ò'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'Oe',
        'Ş'=>'S', 'Ś'=>'S', 'Ș'=>'S', 'Š'=>'S',
        'Ț'=>'T',
        'Ù'=>'U', 'Û'=>'U', 'Ú'=>'U', 'Ü'=>'Ue',
        'Ý'=>'Y',
        'Ź'=>'Z', 'Ž'=>'Z', 'Ż'=>'Z',
        'â'=>'a', 'ǎ'=>'a', 'ą'=>'a', 'á'=>'a', 'ă'=>'a', 'ã'=>'a', 'Ǎ'=>'a', 'а'=>'a', 'А'=>'a', 'å'=>'a', 'à'=>'a', 'א'=>'a', 'Ǻ'=>'a', 'Ā'=>'a', 'ǻ'=>'a', 'ā'=>'a', 'ä'=>'ae', 'æ'=>'ae', 'Ǽ'=>'ae', 'ǽ'=>'ae',
        'б'=>'b', 'ב'=>'b', 'Б'=>'b', 'þ'=>'b',
        'ĉ'=>'c', 'Ĉ'=>'c', 'Ċ'=>'c', 'ć'=>'c', 'ç'=>'c', 'ц'=>'c', 'צ'=>'c', 'ċ'=>'c', 'Ц'=>'c', 'Č'=>'c', 'č'=>'c', 'Ч'=>'ch', 'ч'=>'ch',
        'ד'=>'d', 'ď'=>'d', 'Đ'=>'d', 'Ď'=>'d', 'đ'=>'d', 'д'=>'d', 'Д'=>'D', 'ð'=>'d',
        'є'=>'e', 'ע'=>'e', 'е'=>'e', 'Е'=>'e', 'Ə'=>'e', 'ę'=>'e', 'ĕ'=>'e', 'ē'=>'e', 'Ē'=>'e', 'Ė'=>'e', 'ė'=>'e', 'ě'=>'e', 'Ě'=>'e', 'Є'=>'e', 'Ĕ'=>'e', 'ê'=>'e', 'ə'=>'e', 'è'=>'e', 'ë'=>'e', 'é'=>'e',
        'ф'=>'f', 'ƒ'=>'f', 'Ф'=>'f',
        'ġ'=>'g', 'Ģ'=>'g', 'Ġ'=>'g', 'Ĝ'=>'g', 'Г'=>'g', 'г'=>'g', 'ĝ'=>'g', 'ğ'=>'g', 'ג'=>'g', 'Ґ'=>'g', 'ґ'=>'g', 'ģ'=>'g',
        'ח'=>'h', 'ħ'=>'h', 'Х'=>'h', 'Ħ'=>'h', 'Ĥ'=>'h', 'ĥ'=>'h', 'х'=>'h', 'ה'=>'h',
        'î'=>'i', 'ï'=>'i', 'í'=>'i', 'ì'=>'i', 'į'=>'i', 'ĭ'=>'i', 'ı'=>'i', 'Ĭ'=>'i', 'И'=>'i', 'ĩ'=>'i', 'ǐ'=>'i', 'Ĩ'=>'i', 'Ǐ'=>'i', 'и'=>'i', 'Į'=>'i', 'י'=>'i', 'Ї'=>'i', 'Ī'=>'i', 'І'=>'i', 'ї'=>'i', 'і'=>'i', 'ī'=>'i', 'ĳ'=>'ij', 'Ĳ'=>'ij',
        'й'=>'j', 'Й'=>'j', 'Ĵ'=>'j', 'ĵ'=>'j', 'я'=>'ja', 'Я'=>'ja', 'Э'=>'je', 'э'=>'je', 'ё'=>'jo', 'Ё'=>'jo', 'ю'=>'ju', 'Ю'=>'ju',
        'ĸ'=>'k', 'כ'=>'k', 'Ķ'=>'k', 'К'=>'k', 'к'=>'k', 'ķ'=>'k', 'ך'=>'k',
        'Ŀ'=>'l', 'ŀ'=>'l', 'Л'=>'l', 'ł'=>'l', 'ļ'=>'l', 'ĺ'=>'l', 'Ĺ'=>'l', 'Ļ'=>'l', 'л'=>'l', 'Ľ'=>'l', 'ľ'=>'l', 'ל'=>'l',
        'מ'=>'m', 'М'=>'m', 'ם'=>'m', 'м'=>'m',
        'ñ'=>'n', 'н'=>'n', 'Ņ'=>'n', 'ן'=>'n', 'ŋ'=>'n', 'נ'=>'n', 'Н'=>'n', 'ń'=>'n', 'Ŋ'=>'n', 'ņ'=>'n', 'ŉ'=>'n', 'Ň'=>'n', 'ň'=>'n',
        'о'=>'o', 'О'=>'o', 'ő'=>'o', 'õ'=>'o', 'ô'=>'o', 'Ő'=>'o', 'ŏ'=>'o', 'Ŏ'=>'o', 'Ō'=>'o', 'ō'=>'o', 'ø'=>'o', 'ǿ'=>'o', 'ǒ'=>'o', 'ò'=>'o', 'Ǿ'=>'o', 'Ǒ'=>'o', 'ơ'=>'o', 'ó'=>'o', 'Ơ'=>'o', 'œ'=>'oe', 'Œ'=>'oe', 'ö'=>'oe',
        'פ'=>'p', 'ף'=>'p', 'п'=>'p', 'П'=>'p',
        'ק'=>'q',
        'ŕ'=>'r', 'ř'=>'r', 'Ř'=>'r', 'ŗ'=>'r', 'Ŗ'=>'r', 'ר'=>'r', 'Ŕ'=>'r', 'Р'=>'r', 'р'=>'r',
        'ș'=>'s', 'с'=>'s', 'Ŝ'=>'s', 'š'=>'s', 'ś'=>'s', 'ס'=>'s', 'ş'=>'s', 'С'=>'s', 'ŝ'=>'s', 'Щ'=>'sch', 'щ'=>'sch', 'ш'=>'sh', 'Ш'=>'sh', 'ß'=>'ss',
        'т'=>'t', 'ט'=>'t', 'ŧ'=>'t', 'ת'=>'t', 'ť'=>'t', 'ţ'=>'t', 'Ţ'=>'t', 'Т'=>'t', 'ț'=>'t', 'Ŧ'=>'t', 'Ť'=>'t', '™'=>'tm',
        'ū'=>'u', 'у'=>'u', 'Ũ'=>'u', 'ũ'=>'u', 'Ư'=>'u', 'ư'=>'u', 'Ū'=>'u', 'Ǔ'=>'u', 'ų'=>'u', 'Ų'=>'u', 'ŭ'=>'u', 'Ŭ'=>'u', 'Ů'=>'u', 'ů'=>'u', 'ű'=>'u', 'Ű'=>'u', 'Ǖ'=>'u', 'ǔ'=>'u', 'Ǜ'=>'u', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'У'=>'u', 'ǚ'=>'u', 'ǜ'=>'u', 'Ǚ'=>'u', 'Ǘ'=>'u', 'ǖ'=>'u', 'ǘ'=>'u', 'ü'=>'ue',
        'в'=>'v', 'ו'=>'v', 'В'=>'v',
        'ש'=>'w', 'ŵ'=>'w', 'Ŵ'=>'w',
        'ы'=>'y', 'ŷ'=>'y', 'ý'=>'y', 'ÿ'=>'y', 'Ÿ'=>'y', 'Ŷ'=>'y',
        'Ы'=>'y', 'ž'=>'z', 'З'=>'z', 'з'=>'z', 'ź'=>'z', 'ז'=>'z', 'ż'=>'z', 'ſ'=>'z', 'Ж'=>'zh', 'ж'=>'zh'
    );

  //Remove Extension from filename
    $splitString = explode( ".", $s );
    if(count($splitString) > 1) {   
      $extension = ".".end($splitString);
      unset($splitString[(count($splitString)-1)]);

      $filename = implode( ".", $splitString );
    } else {
      $filename = $s;
      $extension = "";
    }
    
  //Replace filename's characters and capitals
    $nochars_filename = strtr($filename, $replace);
    $newText = iconv(mb_detect_encoding($nochars_filename, mb_detect_order(), true), "UTF-8", $nochars_filename);
    $newText = strtolower($newText);

  //add back the extension
    $final_filename = $newText.$extension;

    return $final_filename;
}

$dir = new DirectoryIterator(dirname(__FILE__));
foreach ($dir as $fileinfo) {
    if (!$fileinfo->isDot()) {
        echo "<ul>";
        echo "<li><h1>".$fileinfo->getPathname()."</h1>";


        if ($fileinfo->isDir()) {
          if ($handle = opendir( $fileinfo->getPathname()) ) {
            while (false !== ($fileName = readdir($handle))) {
        // if(is_dir ( $fileinfo->getPathname()."\\".$fileName )){
              if ($handle2 = opendir( "uploads\\".$fileName) ) {
               if($fileName !== "." && $fileName !== ".." ) {
                echo "<ol>";
                echo "<li><h2>".$fileinfo->getPathname()."\\".$fileName."</h2>";
                while (false !== ($fileName2 = readdir($handle2))) {
                  echo "<ul>";
                  if(!is_dir ( $fileinfo->getPathname()."\\".$fileName )){  

                    echo "<li>";
                    echo $fileName2;
                    $newName2 = normalizeChars($fileName2);
                    if($fileinfo->getPathname()."\\".$fileName."\\".$fileName2 !== $fileinfo->getPathname()."\\".$fileName."\\".$newName2){
                       echo "<h4 style='color:red;'> FLAG|| ";
                       echo $newName2;
                       echo "</h4>";
                       rename($fileinfo->getPathname()."\\".$fileName."\\".$fileName2, $fileinfo->getPathname()."\\".$fileName."\\".$newName2);


                   } else {
                       echo "<br>";
                       echo $newName2;

                   }
                   echo "</li>";

                 } else { //it's another directory -- LEVEL 3
                 if(is_dir ( $fileinfo->getPathname()."\\".$fileName."\\".$fileName2 )){
                    if($fileName2 !== "." && $fileName2 !== ".." ) {
                        if ($handle3 = opendir( "uploads\\".$fileName."\\".$fileName2) ) {
                            echo "<li><h3>".$fileinfo->getPathname()."\\".$fileName."\\".$fileName2."</h3>";
                            echo "<li>third level</li>";
                            echo "<ul>";
                            while (false !== ($fileName3 = readdir($handle3))) {
                              if(is_dir ( $fileinfo->getPathname()."\\".$fileName."\\".$fileName2."\\".$fileName3 )){
                                  if($fileName3 !== "." && $fileName3 !== ".." ) {
                                      if(!is_dir ( $fileinfo->getPathname()."\\".$fileName."\\".$fileName2."\\".$fileName3 )){  
                                        echo "<li>";
                                        echo $fileName3;
                                        $newName3 = normalizeChars($fileName3);
                                        if($fileinfo->getPathname()."\\".$fileName."\\".$fileName2."\\".$fileName3 !== $fileinfo->getPathname()."\\".$fileName."\\".$fileName2."\\".$newName3) {
                                           echo "<h4 style='color:red;'> FLAG|| ";
                                           echo $newName3;
                                           echo "</h4>";
                                           rename($fileinfo->getPathname()."\\".$fileName."\\".$fileName2."\\".$fileName3, $fileinfo->getPathname()."\\".$fileName."\\".$fileName2."\\".$newName3);


                                       } else {
                                           echo "<br>";
                                           echo $newName3;

                                       }
                                       echo "</li>";
                                   }
                          } //not a dot pls
                          } else { //IS NOT A DIR;is a file
                            echo "<li>";
                            echo $fileName3;
                            $newName3 = normalizeChars($fileName3);
                            if($fileinfo->getPathname()."\\".$fileName."\\".$fileName2."\\".$fileName3 !== $fileinfo->getPathname()."\\".$fileName."\\".$fileName2."\\".$newName3) {
                               echo "<h4 style='color:red;'> FLAG|| ";
                               echo $newName3;
                               echo "</h4>";
                               rename($fileinfo->getPathname()."\\".$fileName."\\".$fileName2."\\".$fileName3, $fileinfo->getPathname()."\\".$fileName."\\".$fileName2."\\".$newName3);


                           } else {
                               echo "<br>";
                               echo $newName3;

                           }
                           echo "</li>";
                        } //File level 3
                        } //while 3rd level
                        echo "</ul>";
                        closedir($handle3);
                        echo "</li>";
                    } //open 3rd level dir (2000)
                    } //not a dot pls
                    } else { //IS NOT A DIR;is a file
                        echo "<li>";
                        echo $fileName2;
                        $newName2 = normalizeChars($fileName2);
                        if($fileinfo->getPathname()."\\".$fileName."\\".$fileName2 !== $fileinfo->getPathname()."\\".$fileName."\\".$newName2){
                           echo "<h4 style='color:red;'> FLAG|| ";
                           echo $newName2;
                           echo "</h4>";
                           rename($fileinfo->getPathname()."\\".$fileName."\\".$fileName2, $fileinfo->getPathname()."\\".$fileName."\\".$newName2);


                       } else {
                           echo "<br>";
                           echo $newName2;

                       }
                       echo "</li>";
                    } //File level 2
                }
                echo "</li>";
                echo "</ul>";
            } //while 2nd level
            echo "</ul>";
            closedir($handle2);
            echo "</li>";
            echo "</ol>";
          } //no dot pls
          } //open 2nd level dir (2000)
        // } 
        } //while files
        closedir($handle);
      } //if dirs
  }


  echo "</li>";
  echo "</ul>";
}
}

?>

</body>
</html>