# Image Accents Remover for Media Files on WordPress #

Run this file while the uploads folder from your WordPress website is in the same directory. It'll replace all accented characters by their normal letter equivalent, remove spaces, remove capitals to make the filename perfectly sterile. I also included some foreign characters. Please feel free to add stuff to the mega replace array. 

PS: would this run on a WordPress (as a plugin), I'd run the [sanitize_title_with_dashes](https://codex.wordpress.org/Function_Reference/sanitize_title_with_dashes) function. It'd be probably less of a hassle then.

On WordPress again, when you're done, please make sure this line is present somewhere in your theme or plugins. This will prevent the media library from allowing accents in the future. 


```
#!php

add_filter('sanitize_file_name', 'remove_accents');

```

Important note: **It does NOT change the database entries to match the new file names.** I used a pro version of [Media File Renamer](http://apps.meow.fr/media-file-renamer/). Also look out for [my detailed solution on WP support forums](https://wordpress.org/support/topic/did-everything-right-except-remove-accents-bought-pro-to-manually-override?replies=5).

Might do a quick plugin thingie in the future for WP, so the steps 8 and 9 below will be eliminated. 

### What is this repository for? ###

* People with accents on their images who moved and suddenly they don't work anymore
* I don't know any other people who would need this, but if you do and this isn't your case, let me know

### How do I get set up? ###

1. Make backups of all your images in uploads, and your database, before anything. When I tested, I had to revert about 15 times before I got it right.
2. Make sure you run this on your local machine (wamp xamp whatever). **Do not run on a web server;** it's not optimized for it, it's basically a lot of loops in loops and I fear for your server memory.
3. Do a compressed copy of all your Uploads folder. Don't change the directory logic (keep them in their exact year/months folders). Keep this copy apart in case everything turns wrong. Ideally, zip this from your cPanel/ssh, as FTP softwares like Filezilla has history to ruin accented images when they get transferred, not to mention your very computer.
4 Extract the uploads folder on a new directory on Localhost, so it matches this repo's structure.
5. Copy the image-accent-remover.php file to Localhost and run it on a browser.
6. To make sure the accents were properly removed, before running the PHP file, spot a file with accents in it directly in your Uploads, and then search of the part of it without accents in the PHP generated page in step 6. Example: for a word like Énergie, I'd look for "nergie," until I can find the instance of the file. 
7. All **red Flags** present in the HTML result of the php file are filenames that were modified in one way or another (some of them might just have their capitalized letters removed). Note each instances, so you can know which files to edit in the Media Library afterwards.
8. Sometimes, you'll need to run this multiple times. Some accents are sometimes replaced by weirder looking accents on your computer (especially on Windows). The first time the code will run, it'll return the filename in a good, UTF-8 format, with accents. Then you'll need to re-run the code to transfer this newly generated accent into its accentless equivalent.
9. On WordPress, use [Media File Renamer](http://apps.meow.fr/media-file-renamer/) Pro version and [use the settings as indicated in my explanation](https://wordpress.org/support/topic/did-everything-right-except-remove-accents-bought-pro-to-manually-override?replies=5). **Note that this technique has a LOT of assumptions**, that it is not meant to be foolproof but just a quick measure to fix something annoying. The number 1 assumption is that your **Media File Title is the same as the Media File filename**. Example: my file on my computer is called "Travaux d'Énergie.jpg", the uploaded filename on WordPress was Travaux-d-Énergie.jpg, and its Media Title was Travaux d'Énergie. If you did SEO work on some Media files to have keyword rich titles, but this was done AFTER the upload, those files will need to be treated separately. 
10. If you don't want to pay for the plugin, and you don't that many images to change their accents, you may do so directly in the database too, but that would be time consuming to target each individual Flags you noted in step 6 and change the associated file name. Media files URLs are located in post_meta, and you can get the Media ID by going in the library and noting it from the URL.

Essentially if you need someone to do this job for you, email me at [karine@flaredust.com](karine@flaredust.com) with the amount of media you have (See in your Media Library). For the job I made this code, they had 5 000+ media files of many kinds (pdf, word docs), and some (30-40 files) of them didn't follow the assumption of Media title == filename, because the media had SEO work on them (for good titles). As a comparison, I would have charged $400 CAD for the work, would I have had this tool already done. On another hand, I also had to do this process on 450 Media Files with SEO-boosted Titles, which took me 3hrs of boring work, so if you did change media file titles, hire me instead to finish this plugin at half my rate. It'll save everyone's time.

### Contribution guidelines ###

I'm a 100% noob at repos and stuff so feel free to let me know if you want to merge changes or something. I don't know how this whole repo world works yet, aside from using Source Tree on private repos. 

### Who do I talk to? ###

* Repo owner: Karine F.G. at karine@flaredust.com; English isn't my main language, so I have the tendency to botch it, as you may have noticed, but I'll understand most of what you talk to me about.